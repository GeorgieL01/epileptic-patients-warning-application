import * as React from 'react'
import Layout from '../components/layout'
import { Link, graphql } from "gatsby"
import fireworkVideo from "../videos/fireworks.mp4"


function getImageLightness(imageSrc, callback) {
    var img = document.createElement("img");
    img.src = imageSrc;
    img.style.display = "none";
    document.body.appendChild(img);

    var colorSum = 0;

    img.onload = function () {
        // create canvas
        var canvas = document.createElement("canvas");
        canvas.width = this.width;
        canvas.height = this.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);

        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var data = imageData.data;
        var r, g, b, avg;

        for (var x = 0, len = data.length; x < len; x += 4) {
            r = data[x];
            g = data[x + 1];
            b = data[x + 2];

            avg = Math.floor((r + g + b) / 3);
            colorSum += avg;
        }

        var brightness = Math.floor(colorSum / (this.width * this.height));
        callback(brightness);
    }
}



function Video({
    video: {
        videoScreenshots,
        videoH264,
    },
    ...props
}) {
    let screenshots = videoScreenshots.map(x => x)
    let duration = videoH264.duration
    let lastBrightness = ""

    let flashes = []

    console.log(duration)
    for (var i = 0; i < screenshots.length; i++) {
        let apple = i
        getImageLightness(screenshots[i].publicURL, function (brightness) {
            if (!i == 0) {
                if ((lastBrightness < 30 && brightness > 70) || (lastBrightness > 70 && brightness < 30)) {


                    let a = duration / 10

                    let b = a * apple

                    flashes.push(b)
                    alert("Flash! At: " + b)
                }


            }

            lastBrightness = brightness
        })
    }


    return (

        <div {...props}>
            <div className="screenshots">
                {videoScreenshots.map(({ publicURL }) => (
                    <div key="path">
                        <img src={publicURL} />
                    </div>
                ))}

            </div>

        </div>
    )
}

const LoadVid = ({ data, location }) => {
    let URL = location.state.url.toString();
    return (
        <Layout pageTitle="Watch Video">
            <div>
                <video controls>
                    <source src={fireworkVideo} type="video/mp4" />
                </video>
            </div>
            <Link to="/search">
                <input type="button" value="Return to Search"></input>
            </Link>

            <hr />
            <div>
                <Video video={data.file} />

            </div>
        </Layout>
    )
}

export default LoadVid



export const query = graphql`
query VideoQuery {
  file(relativePath: {eq: "fireworks.mp4"}) {
    videoScreenshots(
      timestamps: ["10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "99%"]
    ) {
      publicURL
    }
    videoH264 {
      duration
    }
  }
}`