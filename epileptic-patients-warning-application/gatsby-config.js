const { resolve } = require(`path`)

module.exports = {
    siteMetadata: {
        siteUrl: "https://www.yourdomain.tld",
        title: "epileptic-patients-warning-application",
    },
    plugins: [
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-transformer-video`,
            options: {
                downloadBinaries: false,
                ffmpegPath: resolve(__dirname, '.bin', 'ffmpeg.exe'),
                ffprobePath: resolve(__dirname, '.bin', 'ffprobe.exe'),
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `videofiles`,
                path: `${__dirname}/src/videos`,
            }

        },
        {
            resolve: `gatsby-source-mysql`,
            options: {
                connectionDetails: {
                    host: 'localhost',
                    user: 'root',
                    password: '',
                    database: 'epilepticpatientwarningdb'
                },
                queries: [
                    {
                        statement: 'SELECT * FROM sysuser',
                        idFieldName: 'id',
                        name: 'sysuser'
                    }
                ]
            }
        },
        {
            resolve: `gatsby-plugin-create-client-paths`,
            options: { prefixes: [`/app/*`] },
        },
    ],

};
